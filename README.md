# Online File Storage Frontend #

This is the frontend for onlinefs backend ([ref here](https://gitlab.com/gigo/online-file-storage))

We host a demo site: [https://www.onlinefs.gigo.rocks](https://www.onlinefs.gigo.rocks)


## Deployment ##

The following instructions is for hosting on google cloud. 

1. setup `docker`
2. create folder `cert` in project root, and put chain certificate and private key inside.
3. build by docker: `docker build -t gcr.io/online-file-storage/frontend:v1`
4. push to google container registry: `gcloud docker push gcr.io/online-file-storage/frontend:v1`
5. (assume we have setup container cluster) run the frontend service with a pod: `kubectl run frontend-pod --image=gcr.io/online-file-storage/frontend:v1`
6. expose it as a service: `kubectl expose deployment frontend-pod --type="LoadBalancer" --port=443`


## Development setup ##

1. install `bower`
2. `bower install`