FROM nginx:1.8.1
MAINTAINER Gigo <demoncris@gmail.com>

COPY app/ /usr/share/nginx/html/
COPY cert/ /etc/onlinefs/
COPY nginx.conf /etc/nginx/conf.d/onlinefs.conf

EXPOSE 433
