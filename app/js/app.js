'use strict';

var onlinefsApp = angular.module('onlinefsApp', [
  'ngRoute',
  'ngStorage',

  'AuthenticationController',
  'FileBrowserController',
  'FileBrowserTabController'
]);

onlinefsApp.config(['$routeProvider', '$httpProvider',
function($routeProvider, $httpProvider) {
    $routeProvider.
    when('/', {
        templateUrl: 'partials/welcome.html',
        controller: 'AuthCtrl'
    }).
    when('/filebrowser', {
        templateUrl: 'partials/filebrowser.html',
        controller: 'FileBrowserTabCtrl'
    }).
    otherwise({
        redirectTo: '/'
    });

    $httpProvider.interceptors.push(['$q', '$location', '$localStorage', function($q, $location, $localStorage) {
        return {
            'request': function (config) {
                config.headers = config.headers || {};
                if ($localStorage.token) {
                    config.headers.Authorization = 'Bearer ' + $localStorage.token;
                }
                return config;
            },
            'responseError': function(response) {
                if(response.status === 401 || response.status === 403) {
                    $location.path('/welcome');
                }
                return $q.reject(response);
            }
        };
    }]);
}]);
