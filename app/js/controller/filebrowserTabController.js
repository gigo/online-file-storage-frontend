'use strict';

angular.module('FileBrowserTabController', ['mgcrea.ngStrap'])
.controller('FileBrowserTabCtrl', ['$scope', function($scope) {
    $scope.tabs = [
        {title:'List', template: 'partials/filelist.html', content: 'List me'},
        {title:'Upload', template: 'partials/fileupload.html', content: 'Upload me'},
    ];

    $scope.tabs.activeTab = 0;
}]);
