'use strict';

angular.module('FileBrowserController', ['ngStorage', 'ngFileUpload', 'ui.bootstrap'])
.controller('FileListCtrl', ['$scope', '$http', '$routeParams', '$localStorage', '$window', '$rootScope', function($scope, $http, $routeParams, $localStorage, $window, $rootScope) {
    var FILE_API = "api/file/";

    $scope.files = [];

    var fetchFileList = function() {
        $http.get(FILE_API).then(
            function(resp) {
                $scope.files = resp.data.sort( function(first, second) {
                    return second.date - first.date;
                });

                $scope.files.map( function(item) {
                    item.date = new Date(item.date).toLocaleString();

                    if (item.tags) {
                        item.tags = item.tags.toString();
                    }
                });
            }, 
            function(resp, status) {
                console.log("fail: " + JSON.stringify(resp) + " / " + status);
            }
        );
    };

    var unbindFileUploaded = $rootScope.$on('FileUploadCtrl.refreshList', function() {
        fetchFileList();
    });
    $rootScope.$on('$destroy', unbindFileUploaded);

    fetchFileList();

    $scope.download = function (fileId) {
        var downloadPath = FILE_API + 'path/' + fileId;

        $http.get(downloadPath).then(
            function(resp) {
                $window.location.href = resp.data;
            },
            function(resp, status) {
                console.log("fail: " + JSON.stringify(resp) + " / " + status);
            }
        );
    };

    $scope.upload = function () {
        var formData = new FormData();
        formData.append('file', this.file);

        var req = {
            method: 'POST',
            url: FILE_API,
            headers:  {
                'Content-Type': undefined
            },
            data: formData
        };

        $http(req).then(
            function(data) {
                console.log("success: " + data);
            }, 
            function(data, status) {
                console.log("fail: " + data + " / " + status);
            }
        );
    };
}])
.controller('FileUploadCtrl', ['$scope', 'Upload', '$rootScope',  function($scope, Upload, $rootScope) {
    var FILE_API = "api/file/";

    $scope.progress = { value: 0 };

    $scope.submit = function() {
        if ($scope.file) {
            $scope.upload($scope.file);
        }
    };

	// upload on file select or drop
	$scope.upload = function (file) {
		Upload.upload({
			url: FILE_API,
			data: {file: file}
		}).then(function (resp) {
            $scope.progress.value = 0;
            $rootScope.$emit('FileUploadCtrl.refreshList');
		}, function (resp) {
            $scope.progress.value = 0;
		}, function (evt) {
			var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            $scope.progress.value = progressPercentage;
		});
	};
}]);
