'use strict';

angular.module('AuthenticationController', [])
.controller('AuthCtrl', ['$scope', '$http', '$location', '$localStorage', function($scope, $http, $location, $localStorage) {
    
    if ($localStorage.token) {
        $location.path('/filebrowser');
    }

    var AUTH_API = "api/auth/";

    $scope.isLogin = function () {
        return $localStorage.token != null;
    };

    $scope.logout = function () {
        delete $localStorage.token;
        delete $localStorage.email;
        $location.path('/');
    };

    var loginComplete = function(resp) {
        $localStorage.token = resp.data.token;
        $localStorage.email = resp.data.email;
        $location.path('/filebrowser');
    };

    $scope.getEmail = function () {
        return $localStorage.email;
    };

    $scope.register = function () {
        var REGISTER_API = AUTH_API + 'register';

        var data = {
            email: $scope.email, 
            password: $scope.password
        };

        $http.post(REGISTER_API, data).then(loginComplete, 
            function(resp, status) {
                console.log("fail: " + JSON.stringify(resp) + " / " + status);
            }
        );
    };

    $scope.login = function () {
        var LOGIN_API = AUTH_API + 'login';
        
        var data = {
            email: $scope.email, 
            password: $scope.password
        };

        $http.post(LOGIN_API, data).then(loginComplete,
            function(resp, status) {
                console.log("fail: " + JSON.stringify(resp) + " / " + status);
            }
        );
    }
}]);


